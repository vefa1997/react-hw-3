import React from 'react';
import './App.css';
import {Link} from 'react-router-dom'

function Nav() {

    const NavStyle = {
        color: 'white'
    };

    return (
        <nav>
            <h4>Logo</h4>

            <ul className="navLink">
                <Link style={NavStyle} to="/favourites">
                <li>Favourites</li>
                </Link>

                <Link style={NavStyle} to="/card">
                <li>Card</li>
                </Link>
            </ul>
        </nav>
    );
}

export default Nav;
