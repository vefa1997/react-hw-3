import React from 'react';
import './App.css';
import Nav from './Nav';
import Favourites from './Favourites';
import Card from './Card';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

function App() {
  return (
      <Router>
    <div className="App">
      <Nav />
      <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/favourites" component={Favourites} />
      <Route path="/card" component={Card} />
      </Switch>

    </div>
      </Router>
  );
}
const Home = (props) => (
    <div>
      <h1>Home Page</h1>

      <div className="CardImg ma2 bg-light-gray dib ">
        <img src="https://joeschmoe.io/api/v1/female/random" alt="Avatar"  />
        <h1> {props.name} </h1>
        <p> {props.text} </p>
        <p> {props.price} </p>
        <span > {props.cardIcon}</span>
        <span > {props.cardIcon}</span>
        <span > {props.cardIcon}</span>
        <span > {props.cardIcon}</span>
        <span > {props.cardIcon}</span>

      
      </div>





    </div>
);

export default App;
